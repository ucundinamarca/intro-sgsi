$('.contenedor').on('click', '.touch', function (e) {
    e.preventDefault();

    let origen = $(this).attr('origen');
    let target = $(this).attr('target');

    // for (i = 0; i < 4; i++) {
    //     slide.push(clase[i]);
    // }   

    console.log(origen)
    console.log(target)

    $('#' + origen).addClass('invisible');
    $('#' + target).removeClass('invisible');
});

// Tip: avoid this ton of code using AniJS ;)

var element = $('#btn_bienvenida');

// when mouseover execute the animation
$('.contenedor').on('mouseover', '.touch', function () {
// element.mouseover(function () {

    // the animation starts
    $(this).addClass('bounce animated');

    // do something when animation ends
    $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {

        // trick to execute the animation again
        $(e.target).removeClass('bounce animated');

    });

});